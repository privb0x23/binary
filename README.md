# Binary

Binary releases. Currently limited to static musl for Linux.

## Licence

[OpenBSD Licence](LICENCE).

Copyright (c) 2020-2023 privb0x23
